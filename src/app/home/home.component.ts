import { Component, OnInit } from '@angular/core';
import { LoginService } from '../guards/login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private loginService : LoginService) { }

  login(){ 
    this.loginService.login();
  }

  logout(){ 
    this.loginService.logout();
  }

  ngOnInit() {
  }

}
