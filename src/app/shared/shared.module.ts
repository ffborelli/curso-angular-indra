import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FundoAmareloDirective } from './diretivas/fundo-amarelo.directive';
import { FocusInputDirective } from './diretivas/focus-input.directive';
import { ReversePipe } from './pipes/reverse.pipe';
import { FilterAlunoPipe } from './pipes/filter-aluno.pipe';
import { MostrarErroComponent } from './forms/mostrar-erro/mostrar-erro.component';
import { DebugCampoComponent } from './forms/debug-campo/debug-campo.component';
import { BaseComponent } from './forms/base/base.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [
    FundoAmareloDirective, 
    FocusInputDirective, 
    ReversePipe, 
    FilterAlunoPipe, 
    MostrarErroComponent, 
    DebugCampoComponent, 
    BaseComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports : [
    FundoAmareloDirective, 
    FocusInputDirective, 
    ReversePipe,
    FilterAlunoPipe,
    MostrarErroComponent,
    DebugCampoComponent,
    BaseComponent
  ],
  providers:[]
})
export class SharedModule { }
