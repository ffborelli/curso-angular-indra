import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterAluno'
})
export class FilterAlunoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value && args)
      return value.filter(a => { return a.toLowerCase().indexOf(args.toLowerCase()) != -1 });
    else 
      return null;
    //OU assim 

    // return value.filter(function (a) {
    //   if (a.toLowerCase().indexOf(args.toLowerCase())== -1){
    //     return false;
    //   }
    //   else{
    //     return true;
    //   }
    // });
  }

}
