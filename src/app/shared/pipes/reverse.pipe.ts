import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    console.log(args);
    let strReverse = '';
    for(let i = value.length; i >= 0; i--){
      strReverse += value.charAt(i);
    }
    return strReverse;
  }

}
