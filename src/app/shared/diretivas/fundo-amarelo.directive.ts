import { Directive , ElementRef, HostListener, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[fundoAmarelo]'
})
export class FundoAmareloDirective {

  constructor(private elementRef : ElementRef, private renderer : Renderer) {
    console.log(this.elementRef);  
   }

   @HostListener ('mouseenter') onMouseEnter(){
      //this.elementRef.nativeElement.style.backgroundColor = 'yellow';
      /*this.renderer.setElementStyle(this.elementRef.nativeElement,
        'background-color',
        'yellow'
      );*/

      this.backgroundcolor = 'yellow';
   }

   @HostListener ('mouseleave') onMouseLeave(){
    //this.elementRef.nativeElement.style.backgroundColor = 'white';
    /*this.renderer.setElementStyle(this.elementRef.nativeElement,
      'background-color',
      'white'
    );*/
    this.backgroundcolor = 'white';
    
  }

  @HostBinding ('style.background-color') backgroundcolor;
  
}
