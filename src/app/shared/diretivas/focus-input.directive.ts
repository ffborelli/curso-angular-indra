import { Directive, HostListener, ElementRef,Renderer } from '@angular/core';

@Directive({
  selector: 'input[focusInput]'
})
export class FocusInputDirective {

  constructor(private elementRef : ElementRef) { }

  
  @HostListener ('mouseenter') onMouseEnter(){
    this.elementRef.nativeElement.focus();
    
 }

 @HostListener ('mouseleave') onMouseLeave(){
  this.elementRef.nativeElement.blur();
}



}
