import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/shared/forms/base/base.component';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css']
})
export class TemplateDrivenComponent 
extends BaseComponent 
implements OnInit {

  emailBiding;
  senhaBiding

  constructor() {
    super();
   }

  ngOnInit() {
    //this.emailBiding = 'fabrizio@grandeporte.com.br';
    this.senhaBiding = '123';
  }

  isMostrarErroPai(campo){
    return ( !campo.valid  && campo.touched  ); 
  }

  onSubmit(meuForm){
    console.log(meuForm);
  }
}
