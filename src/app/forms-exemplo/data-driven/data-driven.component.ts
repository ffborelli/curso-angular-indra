import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-data-driven',
  templateUrl: './data-driven.component.html',
  styleUrls: ['./data-driven.component.css']
})
export class DataDrivenComponent implements OnInit {

  meuform : FormGroup;

  constructor(private formBuilder : FormBuilder) { }

  ngOnInit() {
    this.meuform = this.formBuilder.group(
      { 
        email : [ 'uiyiyi', [ Validators.required,
           Validators.minLength(3), Validators.email]   ]   ,

        senha : [ '', [ Validators.required   ]    ]
      }
    );  
  }

  getControl(nomeControl : string){
    return this.meuform.get(nomeControl);
  }

  onSubmit(){
    console.log(this.meuform);
    // console.log(this.meuform.get('email'));
    // console.log(this.meuform.get('email').value);
  }

}
