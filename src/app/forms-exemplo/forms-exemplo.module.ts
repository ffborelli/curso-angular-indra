import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsExemploRoutingModule } from './forms-exemplo-routing.module';
import { TemplateDrivenComponent } from './template-driven/template-driven.component';
import { DataDrivenComponent } from './data-driven/data-driven.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TemplateDrivenComponent, DataDrivenComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormsExemploRoutingModule,
    SharedModule
  ]
})
export class FormsExemploModule { }
