import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SegundoComponent } from './segundo/segundo.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    SegundoComponent,
    HomeComponent            
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,  
    FormsModule
    //AlunoModule
    //ProfessorModulo
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
