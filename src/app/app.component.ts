import { Component } from '@angular/core';
import { LoginService } from './guards/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title : any = 'appcursoindra';
  mostrarMenu : boolean = false;

  constructor(private loginService : LoginService){
      this.loginService.mostrarMenu.subscribe(
        (autenticado)=>{
          console.log(autenticado);
          this.mostrarMenu = autenticado;
        }
      );
  }

  getValor() : string {
    let nome : string = 'Indra';    
    return 'a';    
  } 
}
