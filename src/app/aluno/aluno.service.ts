import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlunoService {

  alunos = [ 
    'Rafael', 
    'Leandro', 
    'Carlos'
  ];
  //import { HttpClient } from '@angular/common/http';
  constructor(private http : HttpClient) { }
  //http = new HttpClient();
  public getAlunos(){
    return this.alunos;
  }

  public getPokemons(){
    return this.http.get('https://pokeapi.co/api/v2/pokemon');
  }
}
