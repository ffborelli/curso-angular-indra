import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlunoComponent } from './aluno/aluno.component';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';

const routes: Routes = [
  { path: '', component: AlunoComponent },  
  { path: ':id', component: AlunoDetalheComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlunoRoutingModule { }
