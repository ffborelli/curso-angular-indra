import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Pokemon } from 'src/app/shared/models/pokemon';

@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements OnInit {

  name = new FormControl('');

  pokemon : Pokemon ;

  constructor() { 
    this.pokemon = new Pokemon();
  }

  ngOnInit() {
  }
  onSubmit(){
    console.log(this.name.value);
    console.log(this.pokemon);
  }

}
