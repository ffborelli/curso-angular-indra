import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-botao-aluno',
  templateUrl: './botao-aluno.component.html',
  styleUrls: ['./botao-aluno.component.css']
})
export class BotaoAlunoComponent implements OnInit {

  @ViewChild('botaoLegal') botaoLegalComponente : ElementRef;

  constructor() { }

  @Input() mensagem;

  ngOnInit() {
  }

  ngAfterViewInit(){
    console.log(this.botaoLegalComponente);
    this.botaoLegalComponente.nativeElement.innerText = this.mensagem;
  }

  mostrarMensagem(){
    // this.botaoLegalComponente.nativeElement.disabled = true;
    alert(this.mensagem);
  }
}
