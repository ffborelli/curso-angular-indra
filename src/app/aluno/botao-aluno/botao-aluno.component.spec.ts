import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotaoAlunoComponent } from './botao-aluno.component';

describe('BotaoAlunoComponent', () => {
  let component: BotaoAlunoComponent;
  let fixture: ComponentFixture<BotaoAlunoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotaoAlunoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotaoAlunoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
