import { Component, OnInit, ViewChild } from '@angular/core';
import { AlunoService } from '../aluno.service';
import { Pokemon } from 'src/app/shared/models/pokemon';
import { Router } from '@angular/router';
import { HomeComponent } from 'src/app/home/home.component';

@Component({
  selector: 'app-aluno',
  templateUrl: './aluno.component.html',
  styleUrls: ['./aluno.component.css']
})
export class AlunoComponent implements OnInit {

  @ViewChild('componenteBotaoAluno') componenteBotaoAluno;

  alunos;
  pokemons: Pokemon[];

  data = Date.now();

  xss: string;

  pokemonSelecionado: any;

  constructor(private alunoService: AlunoService, private router : Router) {

    //AlunoService alunoService = new AlunoService();
    this.alunos = alunoService.getAlunos();
    alunoService.getPokemons().subscribe(params => {
      console.log(params['results'][1]['name']);
      //console.log(params['count']);
      this.pokemons = params['results'];
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    console.log(this.componenteBotaoAluno);
    this.componenteBotaoAluno.mostrarMensagem();
  }


  onTypeXSS(value) {
    this.xss = value;
  }

  onClickInfo(p) {
    console.log(p);
    this.pokemonSelecionado = p;
  }

  onNovoPokemonPai(e) {
    console.log(e);
    e.forEach(element => {
      this.pokemons.push(element);
    });

  }

  voltarHome() {
    this.router.navigate(['']);
  }

}
