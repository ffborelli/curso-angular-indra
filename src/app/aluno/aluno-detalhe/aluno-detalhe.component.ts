import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Pokemon } from 'src/app/shared/models/pokemon';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
//import { EventEmitter } from 'events';

@Component({
  selector: 'app-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  //template: `<p>eeee</p>`,
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit, OnChanges {

  @Input() pokemonsDetalhes: any;
  @Output() novoPokemon = new EventEmitter();

  subscricao: Subscription;

  constructor(private activatedRoute: ActivatedRoute, private router : Router) {
    console.log(activatedRoute);

  }

  ngOnInit() {
    let id: number;
    this.subscricao = this.activatedRoute.params.subscribe(data => {
      console.log(data['id']);
    });
  }

  ngOnChanges() {
    console.log(this.pokemonsDetalhes);
  }

  ngOnDestroy() {
    this.subscricao.unsubscribe();
  }

  onNovoPokemon() {
    this.novoPokemon.emit(
      [
        {
          name: 'Ratata',
          url: 'http://uol.com.br'
        },
        {
          name: 'Indra',
          url: 'http://uol.com.br'
        }
      ]

    );
  }

  voltarHome() {
    this.router.navigate(['']);
  }

}
