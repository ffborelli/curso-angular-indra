import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunoComponent } from './aluno/aluno.component';
import { AlunoService } from './aluno.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CicloComponent } from './ciclo/ciclo.component';
import { BotaoAlunoComponent } from './botao-aluno/botao-aluno.component';
import { AlunoRoutingModule } from './aluno-routing.module';

@NgModule({
  declarations: [AlunoDetalheComponent, AlunoComponent, AlunoFormComponent, 
    CicloComponent, BotaoAlunoComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AlunoRoutingModule
  ],
  exports:[
    AlunoDetalheComponent, 
    AlunoComponent
  ],
  providers:[
    AlunoService
  ]
})
export class AlunoModule { }
