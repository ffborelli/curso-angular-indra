import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CursoFormComponent } from './curso-form/curso-form.component';

const routes: Routes = [
  {path : '', component : CursoFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CursoRoutingModule { }
