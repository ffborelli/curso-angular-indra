import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  _isAutenticado : boolean = false;
  mostrarMenu = new EventEmitter();
  
  constructor() { 
    this.mostrarMenu.emit(this._isAutenticado);
  }

  isAutenticado(){
    return this._isAutenticado;
  }

  login(){
    console.log('login');
    this._isAutenticado = true;
    this.mostrarMenu.emit(this._isAutenticado);
  }

  logout(){
    console.log('logout');
    this._isAutenticado = false;
    this.mostrarMenu.emit(this._isAutenticado);
  }

}
