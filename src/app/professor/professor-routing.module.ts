import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfessorComponent } from './professor.component';
import { ProfessorFormComponent } from './professor-form/professor-form.component';

const routes: Routes = [
  {path:'', component: ProfessorComponent},
  {path:'novo', component: ProfessorFormComponent},
  {path:':id', component: ProfessorFormComponent}
];
//cursos.grandeporte.com.br:5080/professores
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessorRoutingModule { }
