import { Component, OnInit } from '@angular/core';
import { ProfessorService } from './professor.service';
import { Professor } from '../shared/models/professor';

import { Router } from '@angular/router';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.css']
})
export class ProfessorComponent implements OnInit {

  professores : Professor [] = [];

  constructor(private professorService : ProfessorService,
    private router : Router
    ) { }

  ngOnInit() {
     
      this.professorService.getAllProfessores().subscribe(
        (dado) =>{
          this.professores = dado;
        }
      );
  }

  deleteProfessor(id){

    
    /*let index;
    for (let i = 0; i < this.professores.length ; i++){
      if (this.professores[i].idprofessor == id){
        index = i;
        break;
      }
    }*/

    this.professorService.deleteProfessor(id)
      .subscribe(
        (retorno)=>{

          let index = this.professores
            .findIndex( ( p ) => p.idprofessor == id );
          this.professores.splice(index , 1 );

        }
      );

    

      //this.router.navigate(['professores']);
  }

}
