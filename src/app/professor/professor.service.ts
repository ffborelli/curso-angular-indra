import { Injectable } from '@angular/core';
import { Professor } from '../shared/models/professor';
import { HttpClient } from '@angular/common/http';
//import { HttpClient } from '@angular/common/http';
//import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProfessorService {

  //url : string = 'localhost:8080/professores';
  url : string = 'http://cursos.grandeporte.com.br:5080/professores';
  professores : Professor[] = [];
  /*[
    { id : 1, nome: 'Professor 1', email:'a@a.com' },
    { id : 2, nome: 'Professor 2', email:'a@a.com' }
  ];*/

  constructor(private http: HttpClient) { }

  getAllProfessores(){
    return this.http.get<Professor[]>(this.url);
    //return this.professores;
  }

  getOneProfessor(id){
    return this.http.get<Professor>(`${this.url}/${id}`);
  }

  updateProfessor(professor, id){
    return this.http.patch<Professor>(`${this.url}/${id}`, professor );
  }

  addProfessor(professor){
    return this.http.post<Professor>(`${this.url}`, professor );
  }

  deleteProfessor(id){
    return this.http.delete<Professor>(`${this.url}/${id}`);
  }


}
