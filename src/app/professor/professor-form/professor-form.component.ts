import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { setRootDomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';
import { ProfessorService } from '../professor.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-professor-form',
  templateUrl: './professor-form.component.html',
  styleUrls: ['./professor-form.component.css']
})
export class ProfessorFormComponent implements OnInit {

  frmProfessor : FormGroup;
  id : number;

  constructor(private ar : ActivatedRoute,
          private ps : ProfessorService,
          private formBuilder : FormBuilder,
          private router : Router) { }

  ngOnInit() {

    this.frmProfessor = this.formBuilder.group(
      {
        idprofessor : [ '' , [ ] ],
        nome : [ '' , [ Validators.required  ] ],
        email : [ '' , [ Validators.required ] ]
      }
    );

    this.ar.params.subscribe( 
      (rota)=>{
        console.log(rota.id);
        this.id = rota.id;
        //edicao
        if(rota.id){
          this.ps.getOneProfessor(rota.id).subscribe(
            (professor) =>{
              console.log(professor);
              this.frmProfessor.patchValue(professor);
            }
          );
        }

      }
    );
  }

  onSubmit(){
    //edicao
    if (this.id){
      this.ps.
        updateProfessor(this.frmProfessor.value,this.id)
        .subscribe(
          (professor) =>{
            this.router.navigate(['professores']);
          },
          (error)=>{
            console.log(error);
          }
        );
    }

    //criação
    else {
      this.ps.addProfessor(this.frmProfessor.value).subscribe(
        (dado) =>{
          this.router.navigate(['professores']);
        }
      );
    }
  }

}
