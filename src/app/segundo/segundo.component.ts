import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-segundo',
  templateUrl: './segundo.component.html',
  styleUrls: ['./segundo.component.css']
})
export class SegundoComponent implements OnInit {

  nomeCurso : string = 'Curso de Angular 7';

  isMouseOver : boolean = false;

  constructor() { }

  ngOnInit() {
  }

  clickInput(value){
    //alert(value) ;
    this.nomeCurso = value;

  }

  getAlerta(value){
    console.log (value);
  }

  onMouseEnter(){
    console.log('mouseenter');
    this.isMouseOver = true;
  }
  onMouseLeave(){
    console.log('mouseleave');
    this.isMouseOver = false;
  }

}
