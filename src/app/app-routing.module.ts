import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CicloComponent } from './aluno/ciclo/ciclo.component';
import { HomeComponent } from './home/home.component';
import { AlunoRoutingModule } from './aluno/aluno-routing.module';
import { ProfessorGuardService } from './guards/professor-guard.service';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'alunos', loadChildren : './aluno/aluno.module#AlunoModule' },
  { path: 'forms', 
  loadChildren : './forms-exemplo/forms-exemplo.module#FormsExemploModule' },
  {path: 'professores' , 
    loadChildren: './professor/professor.module#ProfessorModule',
    canActivate:[ProfessorGuardService]
  },
  {path : 'cursos' , loadChildren : './curso/curso.module#CursoModule'}
  //{ path: 'ciclo', component: CicloComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
